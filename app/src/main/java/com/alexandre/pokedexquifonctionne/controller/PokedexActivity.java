package com.alexandre.pokedexquifonctionne.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.test.mock.MockContext;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alexandre.pokedexquifonctionne.service.AsyncGetAllPokemons;
import com.alexandre.pokedexquifonctionne.service.Preferences;
import com.alexandre.pokedexquifonctionne.R;
import com.alexandre.pokedexquifonctionne.model.PokemonModel;
import com.alexandre.pokedexquifonctionne.model.Trainer;
import com.google.gson.Gson;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Affichage de la liste des Pokémon.
 */
public class PokedexActivity extends AppCompatActivity {

    private Preferences preferences;

    private RecyclerView recyclerView;
    private PokemonListAdapter adapter;
    private TextView tvTitle;
    private Button btnReset;

    private ArrayList<PokemonModel> pokemonsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokedex);

        init();
    }

    private void init() {
        preferences = Preferences.getInstance(getApplicationContext());
        // Charge le fichier .json dans un InputStream
        InputStream inputStream = this.getResources().openRawResource(R.raw.pokemon_fr);
        // Envoie à preferences pour désérialisation
        preferences.loadPokedexIndex(inputStream);

        // On fait le lien avec notre RecyclerView
        recyclerView = findViewById(R.id.rvPokemon);
        // On lui fournit l'adapter custom qu'on a fait
        adapter = new PokemonListAdapter(pokemonsList, this);
        recyclerView.setAdapter(adapter);

        // On lui fournit aussi un LayoutManager pour qu'il sache comment afficher ses vues
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        tvTitle = findViewById(R.id.tvTitle);

        // On récupère les infos de l'activité principale
        Bundle data = getIntent().getExtras();
        String nom = preferences.readName();
        String age = preferences.readAge();
        if (data != null) {
            Trainer trainer = data.getParcelable("trainer");
            if (trainer != null) {
                nom = trainer.getNom();
                age = Integer.toString(trainer.getAge());
                preferences.writeName(nom);
                preferences.writeAge(age);
            }
        }
        String header = "Pokédex de " + nom + ", " + age + " ans";
        tvTitle.setText(header);

        // Bouton Réinitialiser, au clic -> reset
        btnReset = findViewById(R.id.btnReset);
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getPokemons();
    }

    /**
     * Récupère les {@link PokemonModel} en asynchrone et les met dans pokemonsList.
     * A chaque Pokémon récupéré, l'adapter est notifié.
     * Voir {@link AsyncGetAllPokemons}.
     */
    private void getPokemons() {
        new AsyncGetAllPokemons(adapter, pokemonsList).execute(1, Preferences.POKEDEX_LENGTH);
    }

    /**
     * Supprime le nom du dresseur et l'état de capture des Pokémon.
     */
    private void reset() {
        preferences.deleteNameFromPreferences();
        preferences.deletePokemonCaptureState();
        Intent toMainActiviy = new Intent(PokedexActivity.this, MainActivity.class);
        startActivity(toMainActiviy);
    }
}
