package com.alexandre.pokedexquifonctionne.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.Toast;

import com.alexandre.pokedexquifonctionne.service.Preferences;
import com.alexandre.pokedexquifonctionne.R;
import com.alexandre.pokedexquifonctionne.model.Trainer;

import org.apache.commons.lang3.StringUtils;

public class MainActivity extends AppCompatActivity {

    Button btnStart;
    EditText etUsername;
    NumberPicker npAge;
    Preferences preferences;
    Spinner spinnerGenerations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    /**
     * Initialise la vue :
     * - Récupère les éléments de la vue
     * - Ajoute un click listener sur le boutton démarrer
     */
    private void init() {
        etUsername = findViewById(R.id.etUsername);
        npAge = findViewById(R.id.npAge);
        npAge.setMinValue(10);
        npAge.setMaxValue(100);
        btnStart = findViewById(R.id.btnStart);
        spinnerGenerations = findViewById(R.id.spinnerGenerations);

        spinnerGenerations.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int size = 0;
                switch (position) {
                    case 0 : size = 151; break;
                    case 1 : size = 251; break;
                    case 2 : size = 386; break;
                    default: size = 151;
                }
                preferences.POKEDEX_LENGTH = size;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                preferences.POKEDEX_LENGTH = 151;
            }
        });

        preferences = Preferences.getInstance(getApplicationContext());

        // Si on a déjà un nom enregistré, on passe à l'activité 2
        if (StringUtils.isNotBlank(preferences.readName())) {
            goToPokedexActivity();
        }

        // On initialise notre click listener sur le bouton démarrer
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v){
                if (validateName()) {
                    String name = StringUtils.trim(etUsername.getText().toString());
                    int age = npAge.getValue();
                    preferences.writeName(name);
                    Intent toPokedexActivity = new Intent(MainActivity.this, PokedexActivity.class)
                            // on peut le passer dans la méthode car c'est un Parcelable
                            .putExtra("trainer", new Trainer(name, age));
                    startActivity(toPokedexActivity);
                    // goToPokedexActivity();
                } else {
                    String text = "Veuillez entrer un nom";
                    Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /**
     * Renvoie true si le nom n'est pas vide.
     * @return true si validation ok, false sinon
     */
    private Boolean validateName() {
        Boolean validation = true;
        String name = etUsername.getText().toString();
        if (StringUtils.isBlank(name)) {
            validation = false;
        }
        return validation;
    }

    /**
     * Lance l'activité Pokédex.
     */
    private void goToPokedexActivity() {
        Intent toPokedexActivity = new Intent(MainActivity.this, PokedexActivity.class);
        startActivity(toPokedexActivity);
    }
}
