package com.alexandre.pokedexquifonctionne.controller;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alexandre.pokedexquifonctionne.R;

/**
 * Exemple de Fragment. Pas encore montré en cours.
 */
@Deprecated
public class DetailedPokemonFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pokemon_fragment, container, false);
    }
}
