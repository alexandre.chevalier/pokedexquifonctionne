package com.alexandre.pokedexquifonctionne.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.alexandre.pokedexquifonctionne.service.Preferences;
import com.alexandre.pokedexquifonctionne.R;
import com.alexandre.pokedexquifonctionne.model.PokemonModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PokemonListAdapter extends RecyclerView.Adapter<PokemonListAdapter.PokemonViewHolder> {
    private List<PokemonModel> pokemons;
    private Context context;
    private Preferences preferences = Preferences.getInstance(null);

    public PokemonListAdapter(List<PokemonModel> pokemons, Context context) {
        this.pokemons = pokemons;
        this.context = context;
    }

    @NonNull
    @Override
    public PokemonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pokemon_list_item, parent, false);
        PokemonViewHolder viewHolder = new PokemonViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PokemonViewHolder holder, int position) {
        holder.bindPokemon(pokemons.get(position));
    }

    @Override
    public int getItemCount() {
        return pokemons.size();
    }

    /**
     * Pour rattacher notre pokemon_list_item.xml à notre code Java.
     */
    public class PokemonViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView name;
        TextView number;
        Switch swCaptured;

        public PokemonViewHolder(final View itemView) {
            super(itemView);
            context = itemView.getContext();
            image = itemView.findViewById(R.id.ivPokemon);
            name = itemView.findViewById(R.id.tvPokemonName);
            number = itemView.findViewById(R.id.tvNumber);
            swCaptured = itemView.findViewById(R.id.swCaptured);
            swCaptured.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    preferences.updateCaptureForPokemon(getAdapterPosition() + 1, isChecked);
                }
            });
        }

        /**
         * Remplit la {@link View} d'une ligne de notre {@link RecyclerView}
         * avec un objet {@link PokemonModel}.
         *
         * @param pokemon Le {@link PokemonModel} à binder
         */
        public void bindPokemon(PokemonModel pokemon) {
            name.setText(pokemon.getName());
            number.setText(Integer.toString(pokemon.getId()));
            swCaptured.setChecked(isCaptured(pokemon.getId()));
            Picasso.get().load(pokemon.getImage()).into(image);
        }

        private boolean isCaptured(int i) {
            SharedPreferences sharedPref = PreferenceManager
                    .getDefaultSharedPreferences(context.getApplicationContext());
            return sharedPref.getBoolean(Integer.toString(i), false);
        }
    }

}
