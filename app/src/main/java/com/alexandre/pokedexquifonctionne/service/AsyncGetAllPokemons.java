package com.alexandre.pokedexquifonctionne.service;

import android.os.AsyncTask;

import com.alexandre.pokedexquifonctionne.model.PokemonModel;
import com.alexandre.pokedexquifonctionne.controller.PokemonListAdapter;

import java.util.ArrayList;

/**
 * Tâche asyncrhone de récupération des Pokémon dans une liste.
 */
public class AsyncGetAllPokemons extends AsyncTask<Integer, Integer, ArrayList<PokemonModel>> {
    private PokemonListAdapter adapter;
    private ArrayList<PokemonModel> pokemonsList;
    private Preferences preferences;

    public AsyncGetAllPokemons(PokemonListAdapter adapter, ArrayList<PokemonModel> list) {
        this.adapter = adapter;
        this.pokemonsList = list;
        this.preferences = Preferences.getInstance(null);
    }

    @Override
    protected ArrayList<PokemonModel> doInBackground(Integer... integers) {
        int start = integers[0];
        int end = integers[1];
        for (int i = start ; i <= end ; i++) {
            pokemonsList.add(getPokemon(i));
            publishProgress(pokemonsList.size());
        }
        return null;
    }

    /**
     * Renvoie l'URL en String du sprite d'un Pokémon en fonction de son id.
     * @param i identifiant du Pokémon
     * @return L'URL sous forme de String du sprite du Pokémon
     */
    private String getSpriteURLFromId(int i) {
        return "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"
                + Integer.toString(i)  +".png";
    }

    /**
     * Récupère un {@link PokemonModel} à partir de son identifiant.
     * @param i identifiant du Pokémon
     * @return Le {@link PokemonModel}
     */
    PokemonModel getPokemon(int i) {
        PokemonModel model = new PokemonModel();
        model.setId(i);
        model.setName(preferences.getPokemonNameById(i));
        model.setImage(getSpriteURLFromId(i));
        return model;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onPostExecute(ArrayList<PokemonModel> pokemons) {
        super.onPostExecute(pokemons);
        this.pokemonsList = pokemons;
    }
}

