package com.alexandre.pokedexquifonctionne.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.alexandre.pokedexquifonctionne.R;
import com.google.gson.Gson;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;

/**
 * Classe utilitaire pour gérer les préférences du Pokédex.
 * La taille du Pokédex, sauvegarder des éléments de configuration du Pokédex, etc.
 */
public final class Preferences {

    public static int POKEDEX_LENGTH = 151;

    private static Preferences instance = null;
    private static Context context = null;
    private static SharedPreferences sharedPreferences = null;

    private static HashMap index = null;

    // Privé pour ne pas être instancié de l'extérieur
    private Preferences() {
        super();
    }

    public static Preferences getInstance(Context ctxt) {
        if (instance == null) {
            instance = new Preferences();
            context = ctxt;
            if(ctxt != null) {
                sharedPreferences = PreferenceManager
                        .getDefaultSharedPreferences(ctxt.getApplicationContext());
            }
        }
        return instance;
    }

    public void writeName(String value) {
        writeString("name", value);
    }

    public String readName() {
        return readString("name");
    }

    public void writeAge(String value) {
        writeString("age", value);
    }

    public String readAge() {
        return readString("age");
    }

    private void writeString(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    private String readString(String key) {
        return sharedPreferences.getString(key, "");
    }

    /**
     * Désérialise l'InputStream en {@link HashMap} de String,String
     * et le charge dans les préférences.
     * @param is Le fichier .json ouvert
     */
    public void loadPokedexIndex(InputStream is) {
        Gson pokemonsJson = new Gson();
        Reader inputStreamReader = new InputStreamReader(is);
        index = pokemonsJson.fromJson(inputStreamReader, new HashMap<String, String>().getClass());
    }

    /**
     * Met à jour l'état de capture du Pokémon numéro id dans les préférences.
     * @param id Le numéro du Pokémon
     * @param state L'état de capture du Pokémon
     */
    public void updateCaptureForPokemon(int id, boolean state) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(Integer.toString(id), state);
        editor.apply();
    }

    /**
     * Renvoie le nom du Pokémon numéro i.
     * Attention : peut renvoyer <code>null</code> !
     *
     * @param i Le numéro du Pokémon
     * @return Le nom du Pokémon i, null si pas trouvé
     */
    public String getPokemonNameById(int i) {
        return String.valueOf(index.get(Integer.toString(i)));
    }

    /**
     * Supprime le nom du dresseur des préférences.
     */
    public void deleteNameFromPreferences() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove("name");
        editor.apply();
    }

    /**
     * Supprime l'état de capture des Pokémon des préférences.
     */
    public void deletePokemonCaptureState() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        for (int i = 1 ; i <= POKEDEX_LENGTH ; i++) {
            editor.remove(Integer.toString(i));
        }
        editor.apply();
    }
}
