package com.alexandre.pokedexquifonctionne.model;

public class PokemonModel {

    private String name;
    private int id;
    private String image;

    public PokemonModel() {
        super();
    }

    public PokemonModel(String name, int id, String image) {
        this.name = name;
        this.id = id;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
