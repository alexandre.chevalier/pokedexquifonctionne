package com.alexandre.pokedexquifonctionne.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Modélisation d'un dresseur Pokémon.
 * Ce modèle est sérialisable (implémente Parcelable).
 */
public class Trainer implements Parcelable {

    private String nom;
    private int age;

    public Trainer(String nom, int age) {
        this.nom = nom;
        this.age = age;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nom);
        dest.writeInt(age);
    }

    public static final Parcelable.Creator<Trainer> CREATOR = new Parcelable.Creator<Trainer>() {
        @Override
        public Trainer createFromParcel(Parcel source) { return new Trainer(source); }
        @Override
        public Trainer[] newArray(int size) { return new Trainer[size]; }
    };

    private Trainer(Parcel in) {
        this.nom = in.readString();
        this.age = in.readInt();
    }

}

