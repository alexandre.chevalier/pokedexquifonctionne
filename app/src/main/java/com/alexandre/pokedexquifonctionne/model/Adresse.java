package com.alexandre.pokedexquifonctionne.model;

import com.google.gson.annotations.SerializedName;

/**
 * Classe pour l'exemple de désérialisation.
 */
public class Adresse {
    int numero;

    String rue;

    @SerializedName("code_postal")
    int codePostal;

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public int getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(int codePostal) {
        this.codePostal = codePostal;
    }

    @Override
    public String toString() {
        return "Adresse{" +
                "numero=" + numero +
                ", rue='" + rue + '\'' +
                ", codePostal=" + codePostal +
                '}';
    }
}
