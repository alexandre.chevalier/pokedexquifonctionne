package com.alexandre.pokedexquifonctionne;

public enum ExtraBetweenActivities {

    NAME("NAME"),

    INFOS("INFOS");

    private String value;

    ExtraBetweenActivities(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
